# 다시 보는 'Mastering Bitcoin'

* Mastering Bitcoin 1/e 번역서 ['비트코인, 블록체인과 금융의 혁신'](http://www.aladin.co.kr/shop/wproduct.aspx?ItemId=68407419) 요약정리
* [Mastering Bitcoin 2/e](https://github.com/bitcoinbook/bitcoinbook) 추가 요약정리


## Table of Contents

* [1장 서론](./chapter01.md)
* [2장 비트코인 작동원리](./chapter02.md)
* [3장 비트코인 클라이언트](./chapter03.md)
