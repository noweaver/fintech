# 2장 비트코인의 작동 원리

## 거래, 블록, 채굴, 블록체인

* 분산된 신뢰 네트워크를 기반
* 다른 여러 참가자들의 상호작용을 통해서 새로운 속성이 생기면서 신뢰가 쌓임

* 블록체인 탐색기(Blockchain Explorer)
  * 비트코인 네트워크와 블록체인을 통해서 거래를 추적하면서 각 단계를 눈으로 확인 할 수 있음
  * 비트코인 검색 엔진 역할하는 웹 애플리케이션
  * 비트코인 주소, 거래내역, 블록 등 찾아 볼 수 있고 관계 및 흐름을 볼 수 있음
  * 인기 있는 블록체인 탐색기
    * [블록체인 인포(Blockchina info)](https://www.blockchain.com/)
    * [BlockCypher Explorer](https://live.blockcypher.com/)
    * [BitPay Insight](https://insight.bitpay.com/)

### 비트코인 개요

* 블록체인 오버뷰
  ![Blockchain Overview](./images/chapter2/bitcoin-overview.png "Blockchain Overview")


### 커피 한 잔 구매하기

* 계산대에서는 두 개의 결제 수단 제공
  * 현금
  * 비트코인
  * POS 시스템에서는 현금 대비 비트코인 시세를 보여줌
  * 지불 요청 QR 코드 제시

    ![QR code](./images/chapter2/qr-code.png "QR code")
  * QR code 안내 내용
    <pre>
    bitcoin:1GdK9UzpHBzqzX2A9JFP3Di4weBwqgmoQA?
    amount=0.015&
    label=Bob%27s%20Cafe&
    message=Purchase%20at%20Bob%27s%20Cafe

    Components of the URL

    A bitcoin address: "1GdK9UzpHBzqzX2A9JFP3Di4weBwqgmoQA"
    The payment amount: "0.015"
    A label for the recipient address: "Bob's Cafe"
    A description for the payment: "Purchase at Bob's Cafe"
    </pre>
  
  * 송금 QR 코드는 목적지 주소만 담겨 있는데 반해 지불요청 QR코드는
    * 인코딩된 URL로서 목적지 주소
    * 지불 금맥,
    * 가게 정보
  * 스마트폰 바코드 스캔

  * 사토시 비트코인
    * 0.00000001 BTC
    * 1 사토시(가장 작은 단위)

## 비트코인 거래

* 거래
  * 비트코인을 많이 보유한 소유주가 비트코인 일부를 다른 사람에 전송하는 것을 승인한다고 네트워크에 알리는 것과 같음
  * 비트코인 소유주는 다른 소유주에세 전송되는 것을 승인하는 다른 거래를 만드는 과정을 반복함으로써 매수한 비트코인을 소비
  * 거래를 통해서 입력값에서 출력값으로 가치가 이동
  
* 코인베이스
  * 마지막 트랜잭션
  * 마이너가 가져가는 코인  

### 일반적인 거래 유형

* 단일거래
  * 하나의 주소에서 다른 주소로 단일거래가 이루어지는 형태
  * 종종 원 소유주에게 돌려줘야 하는 잔액이 존재
  * 하나의 입력값과 두개의 출력값이 발생
  ![단일거래](./images/chapter2/common-transaction.png "단일거래")

* 여러개 입력값을 하나의 출력값으로 합치는 거래
  * 실제로 동전과 단위가 작은 지폐가 많이 있는 경우 큰 단위의 지폐 한 장으로 교환하는 행위와 동일
  * 지불 과정에서 잔액으로 받은 작은 단위의 금액을 정리하기 위해 지갑 애플리케이션에서 때때로 이 유형의 거래가 시행
    * *거래가 발생한다는 것은 마이닝을 한다는 것이고, 수수료가 발생하는 것임*
  ![Aggregation Transaction](./images/chapter2/aggregation-transaction.png "여러개 입력값을 하나의 출력값으로 합치는 거래")

* 하나의 입력값을 여러 명의 수신에게 줄수 있는 여러 개 출력값으로 배분하는 형태
  * 기업 급여
    * *그렇다면 수수료는?*
  ![Distributed Transaction](./images/chapter2/aggregation-transaction.png "하나의 입력값을 여러 명의 수신에게 줄수 있는 여러 개 출력값으로 배분하는 형태")
  

## 거래의 구성

### 올바른 입력값 얻기


### 출력값 생성하기


### 거래내역을 장부에 추가하기

## 비트코인 채굴하기


## 블록에 담겨 있는 거래 채굴하기


## 거래 소비하기
